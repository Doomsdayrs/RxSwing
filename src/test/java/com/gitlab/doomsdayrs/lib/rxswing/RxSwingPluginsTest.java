/*
 * Copyright 2017-2018 David Karnok
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.doomsdayrs.lib.rxswing;

import java.util.concurrent.TimeUnit;

import com.gitlab.doomsdayrs.lib.rxswing.schedulers.SwingSchedulers;
import io.reactivex.rxjava3.core.*;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.internal.functions.Functions;
import io.reactivex.rxjava3.schedulers.Schedulers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RxSwingPluginsTest {

    @Test
    public void utilityClass() {
        TestHelper.checkUtilityClass(RxSwingPlugins.class);
    }

    @AfterEach
    public void after() {
        RxSwingPlugins.reset();
    }

    @Test
    public void onSchedulePassthrough() {
        assertSame(Functions.EMPTY_RUNNABLE, RxSwingPlugins.onSchedule(Functions.EMPTY_RUNNABLE));
    }

    static final class RunnableWrapper implements Runnable {
        final Runnable run;

        RunnableWrapper(Runnable run) {
            this.run = run;
        }

        @Override
        public void run() {
            run.run();
        }
    }

    @Test
    public void onSchedule() {
        RxSwingPlugins.setOnSchedule(RunnableWrapper::new);
        assertNotNull(RxSwingPlugins.getOnSchedule());

        Runnable r = RxSwingPlugins.onSchedule(Functions.EMPTY_RUNNABLE);

        assertTrue( r instanceof RunnableWrapper,r.getClass().toString());

        r.run();

        RxSwingPlugins.reset();

        assertNull(RxSwingPlugins.getOnSchedule());

        assertSame(Functions.EMPTY_RUNNABLE, RxSwingPlugins.onSchedule(Functions.EMPTY_RUNNABLE));
    }

    @Test
    public void onScheduleCrashes() {
        RxSwingPlugins.setOnSchedule(r -> {
            throw new IllegalStateException("Failure");
        });

        try {
            RxSwingPlugins.onSchedule(Functions.EMPTY_RUNNABLE);
            fail("Should have thrown!");
        } catch (IllegalStateException ex) {
            assertEquals("Failure", ex.getMessage());
        }

        RxSwingPlugins.reset();

        assertSame(Functions.EMPTY_RUNNABLE, RxSwingPlugins.onSchedule(Functions.EMPTY_RUNNABLE));
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void onAssembyCrashes() {
        RxSwingPlugins.setOnAssembly(r -> {
            throw new IllegalStateException("Failure");
        });

        try {
            RxSwingPlugins.onAssembly(Observable.just(1));
            fail("Should have thrown!");
        } catch (IllegalStateException ex) {
            assertEquals("Failure", ex.getMessage());
        }

        RxSwingPlugins.reset();

        Observable<Integer> o = Observable.just(1);
        assertSame(o, RxSwingPlugins.onAssembly(o));
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void onAssemby() {
        RxSwingPlugins.setOnAssembly(r -> Observable.just(2));

        assertNotNull(RxSwingPlugins.getOnAssembly());

        Observable<Integer> o = RxSwingPlugins.onAssembly(Observable.just(1));

        o.test().assertResult(2);

        RxSwingPlugins.reset();

        assertNull(RxSwingPlugins.getOnAssembly());

        o = Observable.just(1);
        assertSame(o, RxSwingPlugins.onAssembly(o));
    }

    @Test
    public void onEdtSchedulerCrashes() {
        RxSwingPlugins.setOnEdtScheduler(r -> {
            throw new IllegalStateException("Failure");
        });

        try {
            RxSwingPlugins.onEdtScheduler(Schedulers.computation());
            fail("Should have thrown!");
        } catch (IllegalStateException ex) {
            assertEquals("Failure", ex.getMessage());
        }

        RxSwingPlugins.reset();

        assertSame(Schedulers.computation(), RxSwingPlugins.onEdtScheduler(Schedulers.computation()));
    }

    @Test
    public void onEdtScheduler() {
        RxSwingPlugins.setOnEdtScheduler(r -> Schedulers.computation());
        assertNotNull(RxSwingPlugins.getOnEdtScheduler());

        Observable.just(1)
        .observeOn(SwingSchedulers.edt())
        .map((Function<Integer, Object>) v -> Thread.currentThread().getName().contains("Computation"))
        .test()
        .awaitDone(5, TimeUnit.SECONDS)
        .assertResult(true);

        RxSwingPlugins.reset();

        assertNull(RxSwingPlugins.getOnEdtScheduler());

        Observable.just(1)
        .observeOn(SwingSchedulers.edt())
        .map((Function<Integer, Object>) v -> Thread.currentThread().getName().contains("Computation"))
        .test()
        .awaitDone(5, TimeUnit.SECONDS)
        .assertResult(false);
    }
}
