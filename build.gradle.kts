import java.util.*

val jacocoVersion by project.extra("0.8.9")

plugins {
	`maven-publish`
	signing
	`java-library`
	eclipse
	pmd
	jacoco
	id("ru.vyarus.animalsniffer") version "1.7.0"
	id("com.github.hierynomus.license") version "0.16.1"
	id("biz.aQute.bnd.builder") version "6.4.0"
}

java {
	sourceCompatibility = JavaVersion.VERSION_1_8
	targetCompatibility = JavaVersion.VERSION_1_8
	withSourcesJar()
	withJavadocJar()
}

group = "com.gitlab.doomsdayrs.lib"
version="3.2.0"
status = "release"


if (!hasProperty("mainClass")) {
	val mainClass by extra("")
}

repositories {
	mavenCentral()
}

dependencies {
	api("io.reactivex.rxjava3:rxjava:3.1.6")

	testImplementation("org.junit.jupiter:junit-jupiter-engine:5.9.2")
}

jacoco {
	val jacocoVersion: String by extra
	toolVersion = jacocoVersion // See http://www.eclemma.org/jacoco/.
}

pmd {
	toolVersion = "6.55.0"
	isIgnoreFailures = true
	this.sourceSets = listOf(project.sourceSets.main.get())
	ruleSets = listOf()
	ruleSetFiles = files("pmd.xml")
}

tasks {
	javadoc {
		isFailOnError = false
		(options as StandardJavadocDocletOptions).links(
			"http://docs.oracle.com/javase/17/docs/api/",
		)
	}
	test {
		systemProperty("java.awt.headless", "true")
		maxHeapSize = "2g"
		testLogging {
			events("started", "failed") // "skipped", "passed"
			showStandardStreams = true
			setExceptionFormat("full")
		}
	}
	jacocoTestReport {
		reports {
			xml.required.set(true)
			html.required.set(true)
		}
	}
	pmdMain {
		reports {
			html.required.set(true)
			xml.required.set(true)
		}
	}
	val pmdPrint = register("pmdPrint") {
		dependsOn(pmdMain)
		doLast {
			val file = rootProject.file("build/reports/pmd/main.xml")
			if (file.exists()) {
				println("Listing first 100 PMD violations")

				file.readLines().forEachIndexed { count, line ->
					if (count <= 100) {
						println(line)
					}
				}
			} else {
				println("PMD file not found.")
			}
		}
	}
	check {
		dependsOn(jacocoTestReport, pmdPrint)
	}
	build {
		dependsOn(pmdPrint, jacocoTestReport)
	}
	license {
		header = rootProject.file("HEADER")
		val year by extra(Calendar.getInstance().get(Calendar.YEAR))
		skipExistingHeaders = true
		ignoreFailures = true
		excludes(listOf("**/*.md", "**/*.txt"))
	}
	animalsniffer {
		annotation = "io.reactivex.rxjava3.internal.util.SuppressAnimalSniffer"
	}
	listOf(compileJava, compileTestJava).forEach { task ->
		task.get().options.encoding = "UTF-8"
	}
}

publishing {
	publications {
		create<MavenPublication>("maven") {
			groupId = group.toString()
			artifactId = project.name
			version = project.version.toString()

			from(components["java"])
		}
	}
}